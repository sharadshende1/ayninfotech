import { Injectable } from '@angular/core';
import { Constants } from '../util/Constants';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  stopFlashLoader() {
    const loader = document.getElementById('loader');
    loader.classList.add('fadeOut');
  }

  startFlashLoader() {
    const loader = document.getElementById('loader');
    loader.classList.remove('fadeOut');
  }

  storeData(key: string, value: any) {
    localStorage.setItem(key, value);
  }

  getData(key: string) {
    return localStorage.getItem(key);
  }

  getHeaders() {
    var headers = {}
    headers['content-type'] = 'application/json';
    if (this.getData(Constants.CSURF_TOKEN_NAME))
      headers[Constants.CSURF_TOKEN_NAME] = this.getData(Constants.CSURF_TOKEN_NAME);
    if (this.getData(Constants.SESSION_TOKEN_NAME))
      headers[Constants.SESSION_TOKEN_NAME] = this.getData(Constants.SESSION_TOKEN_NAME);

    return headers;
  }

}
