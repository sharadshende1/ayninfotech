import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { UtilService } from './util.service';
@Injectable() export class HttpService {

  private HOST: string;
  constructor(private http: HttpClient, private utilService: UtilService) {

    this.HOST = "http://localhost:3000/user/";
    
  }

  async get(resourceURI, options: Options) {
    options = this.setHeaders(options);
    return await this.http.get(this.HOST + resourceURI, options).toPromise();
  }

  async post(resourceURI, body, options: Options) {
    options = this.setHeaders(options);
    var result = await this.http.post(this.HOST + resourceURI, body, options).toPromise();
    return result;
  }



  put(resourceURI, body, options: Options) {
    options = this.setHeaders(options);
    return this.http.put(this.HOST + resourceURI, body, options)
  }

  delete(resourceURI, options: Options) {
    options = this.setHeaders(options);
    return this.http.delete(this.HOST + resourceURI, options);
  }

  async head(resourceURI:string) {
    var options = this.setHeaders(undefined);
    options.observe = 'response';
    var result = await this.http.head(this.HOST + resourceURI, { observe: 'response' }).toPromise();
    return result;
  }

  private setHeaders(options): Options {
    if (!options)
      options = new Options();
    if (!options.headers) {
      options.headers = {};
    }
    options.headers['content-type'] = 'application/json';
    options.headers['xsrf-token'] = this.utilService.getData('xsrf-token');
    options.headers['mtg-token'] = this.utilService.getData('mtg-token');
    return options;
  }

}

export class Options {
  params: any;
  headers: any;
  observe: any;
  constructor() {
    this.params = {};
    this.headers = {};
    this.observe = 'body';
  }
}


