import {  Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { UtilService } from '../service/util.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private utilService:UtilService) {
    @ViewChild('canvas', { static: true }) canvas: ElementRef;
   }

  ngOnInit() {
    this.startCamera();
    this.utilService.stopFlashLoader();
  }

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) { 
   navigator.mediaDevices.getUserMedia(this.constraints).then(this).catch(this.handleError);
    } else {
        alert('Sorry, camera not available.');
    }
}

}
