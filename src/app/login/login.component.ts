import { Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import { UtilService } from '../service/util.service';
import { URIConstants } from '../util/URIConstants';
import { LoginRequest } from '../model/request/LoginRequest';
import { Constants } from '../util/Constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ApiResponse } from '../model/response/ApiResponse';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginRequest: LoginRequest;
  loginError: ApiResponse;
  constructor(private httpClient: HttpClient, private utilService: UtilService, private router: Router, private renderer: Renderer2) { }

  ngOnInit() {
  
    this.loginRequest = new LoginRequest();
    this.utilService.stopFlashLoader();
  }




  async doLogin() {
    try {
      this.loginError = undefined;
      var result = await this.httpClient.post<ApiResponse>(URIConstants.LOGIN, this.loginRequest, { observe: 'body', headers: this.utilService.getHeaders() }).toPromise();
      if (result.success) {
        localStorage.setItem(Constants.SESSION_TOKEN_NAME, result.data.accessToken);
        localStorage.setItem("UserName",result.data.companyName)
        this.router.navigateByUrl('/home');
      }
    } catch (error) {
      console.error(":: Error while login :::");
      console.log(this.loginError);
      this.loginError = (error as HttpErrorResponse).error as ApiResponse;
      this.loginRequest.password = 'Admin';
      this.loginRequest.userName = 'Admin123';
    }

  }

}
