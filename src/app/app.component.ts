import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UtilService } from './service/util.service';
import { Constants } from './util/Constants';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = ' Login page';
  isLoggedIn = false;

  constructor(private router: Router, private utilService: UtilService) {
    if (utilService.getData(Constants.SESSION_TOKEN_NAME)) {
      this.isLoggedIn = true;
    }
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (utilService.getData(Constants.SESSION_TOKEN_NAME)) {
          this.isLoggedIn=true;
        }else{
          this.isLoggedIn=false;
        }
        if (val.urlAfterRedirects == '/login') {
          if (this.isLoggedIn) {
            router.navigateByUrl("/home");
          }
        } else if (!this.isLoggedIn) {
          router.navigateByUrl("/login");
        }
      }
    });
  }

  ngOnInit() {
    this.utilService.stopFlashLoader();
  }
}
