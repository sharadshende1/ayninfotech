import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  companyName: string;
  initialLetters: string;
  constructor() { }

  ngOnInit() {
    this.companyName = localStorage.getItem("accountName");
    let arr = this.companyName.split(" ");
    if (arr.length > 1) {
      this.initialLetters=arr[0].charAt(0)+arr[1].charAt(0);
    }else if (arr.length ==1) {
      this.initialLetters=arr[0].charAt(0)+arr[0].charAt(1);
    }
    document.getElementById("nameLogo").setAttribute('data-letters',this.initialLetters.toUpperCase());
  }

}
